const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const mongoose = require('mongoose');
const Task = require('./api/models/todoListModel');
const bodyParser = require('body-parser');

mongoose.Promise = global.Promise;
mongoose.createConnection('mongodb://mongo/local:27017');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const routes = require('./api/routes/todoListRoutes');
routes(app);

app.get('/', (req, res) => {
  res.json({ message: 'At least this works...' });
});

app.listen(port);
console.log(`listening on port ${port}`);